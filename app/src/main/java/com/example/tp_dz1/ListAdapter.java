package com.example.tp_dz1;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

/**
 * Created by mister on 25.10.15.
 */
public class ListAdapter extends ArrayAdapter<ListItem> {
    private final Context context;
    private final List<ListItem> values;

    public ListAdapter(Context context, List<ListItem> values) {
        super(context, -1, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.list_item, parent, false);
        TextView textView = (TextView) rowView.findViewById(R.id.list_item_text);
        ListItem listItem = values.get(position);
        RelativeLayout relativeLayout = (RelativeLayout) rowView.findViewById(R.id.list_item);
        textView.setText(values.get(position).getIdStrFormat());
        relativeLayout.setBackgroundColor(ContextCompat.getColor(getContext(), listItem.getId() % 2 == 0 ? R.color.grey : R.color.white));
        return rowView;
    }
}
