package com.example.tp_dz1;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by mister on 25.10.15.
 */
public class ListItem {
    private int id;
    private String idStr;

    private static Map <Integer, String> map;

    public static void initMap() {
        map = new HashMap<>();

        map.put(1,"один");
        map.put(2,"два");
        map.put(3,"три");
        map.put(4,"четыре");
        map.put(5,"пять");
        map.put(6,"шесть");
        map.put(7,"семь");
        map.put(8,"восемь");
        map.put(9,"девять");
        map.put(10,"десять");
        map.put(11,"одиннадцать");
        map.put(12,"двенадать");
        map.put(13,"тринадать");
        map.put(14,"четырнадать");
        map.put(15,"пятнадцать");
        map.put(16,"шестнадацть");
        map.put(17,"семнадать");
        map.put(18,"восемнадать");
        map.put(19,"девятнадать");
        map.put(20,"двадцать");
        map.put(30,"тридать");
        map.put(40,"сорок");
        map.put(50,"пятьдесят");
        map.put(60,"шестьдесят");
        map.put(70,"семьдесят");
        map.put(80,"восемьдесят");
        map.put(90,"девяносто");
        map.put(100,"сто");
        map.put(200,"двести");
        map.put(300,"триста");
        map.put(400,"четыреста");
        map.put(500,"пятьсот");
        map.put(600,"шестьсот");
        map.put(700,"семьсот");
        map.put(800,"восемьсот");
        map.put(900,"девятьсот");
        map.put(1000, "тысяча");
    }


    public ListItem(int id) {
        setId(id);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
        idStr = getStrOfId(id);
    }

    public String getIdStrFormat() {
        return idStr;
    }

    private String getStrOfId(Integer n) {

        LinkedList<Integer> roundIntList = new LinkedList<>();
        int multi = 1;

        if ((n % 100 >= 11) && (n % 100 <= 19)) {
            multi = 100;
            roundIntList.push(n % 100);
            n /= 100;
        }

        for (; n > 0; multi *= 10) {
            int RoundN;
            RoundN = n % 10 * multi;
            if (RoundN != 0) {
                roundIntList.push(RoundN);
            }
            n /= 10;
        }

        List<String> stringsList = new ArrayList<>();
        while (roundIntList.size() > 0) {
            stringsList.add(map.get(roundIntList.pop()));
        }

        return TextUtils.join(" ", stringsList);
    }
}
