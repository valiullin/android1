package com.example.tp_dz1;

import android.app.ListActivity;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class ListItemActivity extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        List<ListItem> list = new ArrayList<>();
        ListItem.initMap();
        generateIds(list);

        ListAdapter listAdapter = new ListAdapter(this, list);
        setListAdapter(listAdapter);
    }

    private void generateIds(List<ListItem> list) {
        for (int i = 1; i <= 1000; ++i) {
            list.add(new ListItem(i));
        }
    }

}
