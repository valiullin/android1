package com.example.tp_dz1;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {
    private MyTask myTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        myTask = new MyTask();
        myTask.execute();

    }

    @Override
    protected void onStop() {
        super.onStop();
        myTask.cancel(true);
        finish();
    }

    class MyTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            runActivity2();
        }
    }

    void runActivity2() {
        Intent intent = new Intent(this, ListItemActivity.class);
        startActivity(intent);
        finish();
    }
}
